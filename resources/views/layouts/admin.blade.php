<!DOCTYPE html>
<html >
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>CMS - @yield('title')</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
	{{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-rtl/3.4.0/css/bootstrap-rtl.css" rel="stylesheet"> --}}
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="{{ asset('css/datepicker3.css') }}" rel="stylesheet">
	<link href="{{ asset('css/styles.css') }}" rel="stylesheet">
	
	<!--Custom Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<link href="http://fonts.googleapis.com/earlyaccess/notokufiarabic.css" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
</head>
<body class="is-rtls">
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		@include('admin.parts.navbar')
	</nav>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		@include('admin.parts.sidebar')
	</div>
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
	      <ol class="breadcrumb">
	        <li><a href="#">
	          <em class="fa fa-home"></em>
	        </a></li>
	        @yield('breadcrumb')
	      </ol>
	    </div><!--/.row-->
	    <div class="row">
	      <div class="col-lg-12">
	        <h1 class="page-header">@yield('title')</h1>
	      </div>
	    </div><!--/.row-->
		@if($flash = session('success'))
		    <div class="alert alert-success" >
		      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		      {{ $flash }}
		    </div>
		@endif
		@if($flash = session('warning'))
		    <div class="alert alert-warning" >
		      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		      {{ $flash }}
		    </div>
		@endif
	    
		@yield('content')
	</div>	<!--/.main-->
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.min.js"></script>
	<script src="{{ asset('js/chart-data.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/easy-pie-chart/2.1.6/jquery.easypiechart.min.js"></script>
	<script src="{{ asset('js/easypiechart-data.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
	<script src="{{ asset('js/custom.js')}}"></script>
	<script>
// 		window.onload = function () {
// 	// var chart1 = document.getElementById("line-chart").getContext("2d");
// 	// window.myLine = new Chart(chart1).Line(lineChartData, {
// 	// responsive: true,
// 	// scaleLineColor: "rgba(0,0,0,.2)",
// 	// scaleGridLineColor: "rgba(0,0,0,.05)",
// 	// scaleFontColor: "#c5c7cc"
// 	// });
// };
	</script>
		
</body>
</html>