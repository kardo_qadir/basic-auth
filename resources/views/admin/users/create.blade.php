@extends('layouts.admin')
@section('title','Add User')

@section('content')
@section('breadcrumb')
  <li>User</li>
  <li class="active">Add</li>
@endsection
@include ('admin.parts.errors')
<form role="form" action="{{route('users.store')}}" method="POST">
{{csrf_field()}}
<div class="panel panel-default">
  <div class="panel-heading">
    <div class="row">
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <h3 class="title">Create New User</h3>
      </div>
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
        <div class="btn-lh">
           <button type="submit" class="btn btn-primary"><i class="fa fa-user-plus"></i> Add</button>
        </div>
      </div>
    </div>
  </div>
  <div class="panel-body">
    <div class="col-md-6">
      <div class="form-group">
        <label>Name</label>
        <input class="form-control" placeholder="Name" name="name" id="name"  value="{{old('name')}}" >
      </div>

      <div class="form-group">
        <label>Password:</label>
        <input class="form-control" placeholder="password" name="password" id="password" type="password">
      </div>
      <div class="form-group">
        <label>Confirm Password:</label>
        <input class="form-control" placeholder="password" name="password_confirmation" id="password" type="password">
      </div>

    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>Email:</label>
        <input class="form-control" placeholder="email" name="email" id="email" type="email"  value="{{old('email')}}" >
      </div>

      <div class="form-group">
        <label>Roles:</label>
        @foreach ($roles as $role)
          <div class="checkbox">
            <label>
              <input type="checkbox" name="roles[]" value="{{$role->id}}">{{$role->display_name}}
            </label>
          </div>
        @endforeach
      </div>
    </div>
  </div>
</div>
</form>
@endsection


