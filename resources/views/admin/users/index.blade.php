@extends('layouts.admin')
@section('title','List of Users')

@section('content')
    @section('breadcrumb')
      <li class="active">User Settings</li>
    @endsection
    
<div class="panel panel-default articles">
    <div class="panel-heading">
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <h3 class="title">Manage Users</h3>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
          <div class="btn-lh">
            <a href="{{route('users.create')}}" class="btn-primary btn"><i class="fa fa-user-plus m-r-10"></i> Create New User</a>
          </div>
        </div>
      </div>
      </div>
      <div class="panel-body">
    <div class="table-hl">
      <table class="table table-hover">
        <thead>
          <tr>
            <th>id</th>
            <th>Name</th>
            <th>Email</th>
            <th>Date Created</th>
            <th></th>
          </tr>
        </thead>

        <tbody>
          @foreach ($users as $user)
            <tr>
              <th>{{$user->id}}</th>
              <td>{{$user->name}}</td>
              <td>{{$user->email}}</td>
              <td>{{$user->created_at->toFormattedDateString()}}</td>
              <td class="text-left">
                <a class='btn btn-info btn-xs' href="{{route('users.show', $user->id)}}" ><i class="fa fa-eye"></i> View</a>
                <a class="btn btn-success btn-xs" href="{{route('users.edit', $user->id)}}"><i class="fa fa-edit"></i> Edit</a>
                <form class="dsinb" method="POST" action="{{route('users.destroy', $user->id)}}">
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button class=" btn btn-danger btn-xs"><i class="fa fa-close"></i> Delete</button>
                </form>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div></div>
      {{$users->links()}}
@endsection
