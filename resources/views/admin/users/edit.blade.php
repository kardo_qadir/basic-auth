@extends('layouts.admin')
@section('title','Add User')

@section('content')
@section('breadcrumb')
  <li>User</li>
  <li class="active">Edit</li>
@endsection
@include ('admin.parts.errors')
<form role="form" action="{{route('users.update', $user->id)}}" method="POST">
{{method_field('PUT')}}
{{csrf_field()}}
<div class="panel panel-default">
  <div class="panel-heading">
    <div class="row">
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <h3 class="title">Edit User</h3>
      </div>
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
        <div class="btn-lh">
           <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Update</button>
        </div>
      </div>
    </div>
  </div>
  <div class="panel-body">
    <div class="col-md-6">
      <div class="form-group">
        <label>Name</label>
        <input class="form-control" placeholder="Name" name="name" id="name" value="{{$user->name}}">
      </div>

      <div class="form-group">
        <label>Email:</label>
        <input class="form-control" placeholder="email" name="email" id="email" type="email" value="{{$user->email}}">
      </div>

      

    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>Roles:</label>
        @foreach ($roles as $role)
          <div class="checkbox">
            <label>
              <input type="checkbox" name="roles[]" {{$user->checked($role->id)}} value="{{$role->id}}">{{$role->display_name}}
            </label>
          </div>
        @endforeach
      </div>
    </div>
  </div>
</div>
</form>
@endsection


