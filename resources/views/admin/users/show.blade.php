@extends('layouts.admin')
@section('title','User Info')

@section('content')
@section('breadcrumb')
  <li>User</li>
  <li class="active">Detail</li>
@endsection
<div class="panel panel-default">
<div class="panel-heading">
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <h3 class="title">Details</h3>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
          <div class="btn-lh">
            <a href="{{route('users.edit', $user->id)}}" class="btn-success btn"><i class="fa fa-edit "></i> Edit User</a>
          </div>
        </div>
      </div>
</div>
<div class="panel-body">
  <form class="form-horizontal" action="" method="post">
    <fieldset>
      <!-- Name input-->
      <div class="form-group">
        <label class="col-md-3 control-label" for="name">Name</label>
        <div class="col-md-9">
          <pre>{{$user->name}}</pre>
        </div>
      </div>
    
      <!-- Email input-->
      <div class="form-group">
        <label class="col-md-3 control-label" for="email">E-mail</label>
        <div class="col-md-9">
         <pre>{{$user->email}}</pre>
        </div>
      </div>
      
      <!-- Message body -->
      <div class="form-group">
        <label class="col-md-3 control-label" for="message">Roles</label>
        <div class="col-md-9">
            <ul>
              @forelse ($user->roles as $role)
                <li>{{$role->display_name}} ({{$role->description}})</li>
              @empty
                <p>This user has not been assigned any roles yet</p>
              @endforelse
            </ul>
        </div>
      </div>
    </fieldset>
  </form>
</div>
</div>
@endsection
