@extends('layouts.admin')
@section('title','Roles')

@section('content')
@section('breadcrumb')
  <li class="active">Roles</li>
@endsection
<div class="panel  panel-default">
  <div class="panel-heading">
  <div class="row">
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
      <h3 class="title">Manage Roles</h3>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
      <div class="btn-lh"><a href="{{route('roles.create')}}" class="btn btn-primary "><i class="fa fa-user-plus"></i> Create New Role</a></div>
    </div>
  </div>
</div>
<div class="panel-body">
  <div class="row">
    @foreach ($roles as $role)
      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
        <div class="box">
          <article class="panel panel-default" style='border:1px solid #ddd'>
              <div class="panel-body">
              <div class="content">
                <h3 class="title">{{$role->display_name}}</h3>
                <h4 class="subtitle"><em>{{$role->name}}</em></h4>
                <p>
                  {{$role->description}}
                </p>
              </div>

              <div class="buttons">
                <div class="dsinb">
                  <a href="{{route('roles.show', $role->id)}}" class="btn btn-primary"><i class="fa fa-info"></i> Details</a>
                </div>
                <div class="dsinb">
                  <a href="{{route('roles.edit', $role->id)}}" class="btn btn-success"><i class="fa fa-edit"></i> Edit</a>
                </div>
                <form class="dsinb" method="POST" action="{{route('roles.destroy', $role->id)}}">
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button class=" btn btn-danger"><i class="fa fa-close"></i> Delete</button>
                </form>
              </div>
            </div>
          </article>
        </div>
      </div>
    @endforeach
  </div>
  </div>
  </div>
@endsection
