@extends('layouts.admin')
@section('title','Role Detail')

@section('content')
@section('breadcrumb')
  <li>Role</li>
  <li class="active">Detail</li>
@endsection

<div class="panel panel-default">
<div class="panel-heading">
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <h3 class="title">Role Info</h3>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
          <div class="btn-lh">
            <a href="{{route('roles.edit', $role->id)}}" class="btn-success btn"><i class="fa fa-edit "></i> Edit Role</a>
          </div>
        </div>
      </div>
</div>

<div class="panel-body">
  <form action="" method="post">
    <fieldset>
      <!-- Name input-->
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
      <div class="form-group">
          <h3 class="title">{{$role->display_name}} <small class="m-l-25"><em>({{$role->name}})</em></small></h3>
          <h5>{{$role->description}}</h5>
      </div>
    </div>

    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
      
      <!-- Message body -->
      <div class="form-group">
        <label class="col-md-3 control-label" for="message">Role Permissions</label>
        <div class="col-md-9">
            <ul>
              @foreach ($role->permissions as $r)
                <li>{{$r->display_name}} <em class="m-l-15">({{$r->description}})</em></li>
              @endforeach
            </ul>
        </div>
      </div>
    </div>
    </fieldset>
  </form>
</div>
</div>
@endsection



@section('content')
  <div class="flex-container">
    <div class="columns m-t-10">
      <div class="column">
        <h1 class="title">{{$role->display_name}}<small class="m-l-25"><em>({{$role->name}})</em></small></h1>
        <h5>{{$role->description}}</h5>
      </div>
      <div class="column">
        <a href="{{route('roles.edit', $role->id)}}" class="button is-primary is-pulled-right"><i class="fa fa-user-plus m-r-10"></i> Edit this Role</a>
      </div>
    </div>
    <hr class="m-t-0">

    <div class="columns">
      <div class="column">
        <div class="box">
          <article class="media">
            <div class="media-content">
              <div class="content">
                <h2 class="title">Permissions:</h1>
                <ul>
                  @foreach ($role->permissions as $r)
                    <li>{{$r->display_name}} <em class="m-l-15">({{$r->description}})</em></li>
                  @endforeach
                </ul>
              </div>
            </div>
          </article>
        </div>
      </div>
    </div>
  </div>
@endsection
