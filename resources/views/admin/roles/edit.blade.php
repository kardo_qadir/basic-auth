@extends('layouts.admin')
@section('title','Edit Role')

@section('content')
@section('breadcrumb')
  <li>Role</li>
  <li class="active">Edit</li>
@endsection
@include ('admin.parts.errors')
<form role="form" action="{{route('roles.update', $role->id)}}" method="POST">
{{method_field('PUT')}}
{{csrf_field()}}
<div class="panel panel-default">
  <div class="panel-heading">
    <div class="row">
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <h3 class="title">Edit Role</h3>
      </div>
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
        <div class="btn-lh">
           <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Update</button>
        </div>
      </div>
    </div>
  </div>
  <div class="panel-body">
    <div class="col-md-6">
      <div class="form-group">
        <label>Name (Human Readable)</label>
        <input class="form-control" placeholder="Name" name="display_name" id="name" value="{{$role->display_name}}">
      </div>

      <div class="form-group">
        <label>Slug (Can not be edited)</label>
        <input class="form-control" placeholder="email" name="name"  value="{{$role->name}}" disabled id="name">
      </div>
      <div class="form-group">
        <label>Description</label>
        <textarea rows="3" class="form-control"  id="description" name="description">{{$role->description}}</textarea>
      </div>

      

    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>Roles:</label>
        @foreach ($permissions as $permission)
          <div class="checkbox">
            <label>
              <input type="checkbox" name="permissions[]" {{$role->checked($permission->id)}} value="{{$permission->id}}">{{$permission->display_name}}
            </label>
          </div>
        @endforeach
      </div>
    </div>
  </div>
</div>
</form>
@endsection

