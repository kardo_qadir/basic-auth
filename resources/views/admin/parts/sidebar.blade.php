	<div class="profile-sidebar">
		<div class="profile-userpic">
			<img src="{{ Auth::user()->getAvatar(50) }}" class="img-responsive" alt="">
		</div>
		<div class="profile-usertitle">
			<div class="profile-usertitle-name">{{ Auth::user()->name }}</div>
			<div class="profile-usertitle-status"><span class="indicator label-success"></span>Online</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="divider"></div>
	<form role="search">
		<div class="form-group">
			<input type="text" class="form-control" placeholder="Search">
		</div>
	</form>
	<ul class="nav menu">
		{{-- <li class="{{ Request::path() == 'admin/dashboard' ? 'active' : '' }}">
			<a href="{{ route('admin.dashboard') }}"><em class="fa fa-dashboard">&nbsp;</em> ده‌ستپێك</a>
		</li> --}}
		<li class="{{ Request::path() == 'admin/dashboard' ? 'active' : '' }}">
			<a href="{{ route('admin.dashboard') }}"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a>
		</li>

		<li <li class="{{ (Request::path() == 'admin/users' || Request::path() == 'admin/roles' || Request::path() == 'admin/permissions' ) ? 'active' : '' }} parent"><a data-toggle="collapse" href="{{route('users.index')}}" class="{{Nav::isResource('users')}}">
			<em class="fa fa-users">&nbsp;</em> User Settings <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
			{{-- <em class="fa fa-users">&nbsp;</em> ڕێكخستنی به‌كارهێنه‌ران <span data-toggle="collapse" href="#sub-item-1" class="icon pull-left"><em class="fa fa-plus"></em></span> --}}
			</a>
			<ul class="children collapse" id="sub-item-1">
				<li><a href="{{route('users.index')}}" class="{{Nav::isResource('users')}}"><span class="fa fa-user">&nbsp;</span>  به‌كارهێنه‌ران</a></li>
				<li><a href="{{route('roles.index')}}" class="{{Nav::isResource('roles')}}"><span class="fa fa-cog">&nbsp;</span> پله‌كان</a></li>
				<li><a href="{{route('permissions.index')}}" class="{{Nav::isResource('permissions')}}"><span class="fa fa-hourglass">&nbsp;</span> ده‌سه‌ڵاته‌كان</a></li>
			</ul>
		</li>
		<li>
        <a href="{{ route('logout') }}"
            onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
           <em class="fa fa-power-off">&nbsp;</em> Logout
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
	 </li>
	</ul>
</div><!--/.sidebar-->