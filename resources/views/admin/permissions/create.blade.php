@extends('layouts.admin')
@section('title','Add Permission')

@section('content')
@section('breadcrumb')
  <li>Permissions</li>
  <li class="active">Add</li>
@endsection
@include ('admin.parts.errors')
<form role="form" action="{{route('permissions.store')}}" method="POST">
{{csrf_field()}}
<div class="panel panel-default">
  <div class="panel-heading">
    <div class="row">
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <h3 class="title">Add a Permission</h3>
      </div>
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
        <div class="btn-lh">
           <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Create New Permission</button>
        </div>
      </div>
    </div>
  </div>
  <div class="panel-body">
    <div class="col-md-6">
      <div class="form-group">
        <label>Name (Human Readable)</label>
        <input class="form-control" placeholder="Name" name="display_name" id="name" value="{{old('display_name')}}">
      </div>

      <div class="form-group">
        <label>Slug </label>
        <input class="form-control" placeholder="Slug" name="name"  value="{{old('name')}}" id="name">
      </div>

    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>Description</label>
        <textarea rows="6" class="form-control"  id="description" name="description">{{old('description')}}</textarea>
      </div>
    </div>
  </div>
</div>
</form>
@endsection