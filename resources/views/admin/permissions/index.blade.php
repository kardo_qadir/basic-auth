@extends('layouts.admin')
@section('title','Permissions')

@section('content')
  @section('breadcrumb')
    <li>User</li>
    <li class="active">Permissions</li>
  @endsection
    
<div class="panel panel-default articles">
    <div class="panel-heading">
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <h1 class="title">Manage Permissions</h1>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
          <div class="btn-lh">
            <a href="{{route('permissions.create')}}" class="btn-primary btn"><i class="fa fa-user-plus m-r-10"></i> Create New Permission</a>
          </div>
        </div>
      </div>
    </div>
    <div class="panel-body">
      <div class="table-hl">
        <table class="table table-hover ">
          <thead>
            <tr>
              <th>Name</th>
              <th>Slug</th>
              <th>Description</th>
              <th></th>
            </tr>
          </thead>

          <tbody>
            @foreach ($permissions as $permission)
              <tr>
                <th>{{$permission->display_name}}</th>
                <td>{{$permission->name}}</td>
                <td>{{$permission->description}}</td>
                
                <td class="text-left">
                  <a class='btn btn-info btn-xs' href="{{route('permissions.show', $permission->id)}}" ><i class="fa fa-eye"></i> View</a>
                  <a class="btn btn-success btn-xs" href="{{route('permissions.edit', $permission->id)}}"><i class="fa fa-edit"></i> Edit</a>
                  <form class="dsinb" method="POST" action="{{route('permissions.destroy', $permission->id)}}">
                      <input type="hidden" name="_method" value="DELETE">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <button class=" btn btn-danger btn-xs"><i class="fa fa-close"></i> Delete</button>
                  </form>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
</div>
@endsection
