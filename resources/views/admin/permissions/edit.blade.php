@extends('layouts.admin')
@section('title','Edit Permission')

@section('content')
@section('breadcrumb')
  <li>Permission</li>
  <li class="active">Edit</li>
@endsection
@include ('admin.parts.errors')
<form role="form" action="{{route('permissions.update', $permission->id)}}" method="POST">
{{method_field('PUT')}}
{{csrf_field()}}
<div class="panel panel-default">
  <div class="panel-heading">
    <div class="row">
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <h3 class="title">Edit Permission</h3>
      </div>
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
        <div class="btn-lh">
           <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Update</button>
        </div>
      </div>
    </div>
  </div>
  <div class="panel-body">
    <div class="col-md-6">
      <div class="form-group">
        <label>Name (Display Name)</label>
        <input class="form-control" placeholder="Name" name="display_name" id="display_name" value="{{$permission->display_name}}">
      </div>

      <div class="form-group">
        <label>Slug <small>(Cannot be changed)</small></label>
        <input class="form-control"  name="name" id="name" value="{{$permission->name}}" disabled>
      </div>


    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>Description</label>
        <textarea rows="6" class="form-control"  id="description" name="description">{{$permission->description}}</textarea>
      </div>
    </div>
  </div>
</div>
</form>
@endsection

