@extends('layouts.admin')
@section('title','Permission Detail')

@section('content')
@section('breadcrumb')
  <li>Permission</li>
  <li class="active">Detail</li>
@endsection

<div class="panel panel-default">
<div class="panel-heading">
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <h3 class="title">Permission Detail</h3>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
          <div class="btn-lh">
            <a href="{{route('permissions.edit', $permission->id)}}" class="btn-success btn"><i class="fa fa-edit "></i> Edit Permission</a>
          </div>
        </div>
      </div>
</div>

<div class="panel-body">
  <form action="" method="post">
    <fieldset>
      <!-- Name input-->
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
      <div class="form-group">
          <h3 class="title">{{$permission->display_name}} <small class="m-l-25"><em>({{$permission->name}})</em></small></h3>
          <h5>{{$permission->description}}</h5>
      </div>
    </div>

    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
      
      <!-- Message body -->
      
    </div>
    </fieldset>
  </form>
</div>
</div>
@endsection


