<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;

class User extends Authenticatable
{
    use Notifiable;
    use LaratrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function getAvatar($size = 40)
    {
        return 'https://www.gravatar.com/avatar/'.md5($this->email).'?d=mm&s='.$size;
    }


    public function checked($allRole)
    {
        if($this->Roles()->get()->count()){
            $data = '';
            foreach ($this->Roles()->get() as $role) {
                if($role->id == $allRole) $data = 'checked';
            }
            return $data;
        }
    }
}
