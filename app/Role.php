<?php

namespace App;

use Laratrust\LaratrustRole;

class Role extends LaratrustRole
{
    public function checked($allPerms)
    {
        if($this->Permissions()->get()->count()){
            $data = '';
            foreach ($this->Permissions()->get() as $permission) {
                if($permission->id == $allPerms) $data = 'checked';
            }
            return $data;
        }
    }
}
